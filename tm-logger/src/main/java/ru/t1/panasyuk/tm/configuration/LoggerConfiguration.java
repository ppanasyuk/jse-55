package ru.t1.panasyuk.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.panasyuk.tm.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.panasyuk.tm")
public class LoggerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    public ConnectionFactory factory() {
        @NotNull final String activeMqHost = propertyService.getActiveMqHost();
        @NotNull final String activeMqPort = propertyService.getActiveMqPort();
        @NotNull final String Url = "failover://tcp://" + activeMqHost + ":" + activeMqPort;
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(Url);
        factory.setTrustAllPackages(true);
        return factory;
    }

}