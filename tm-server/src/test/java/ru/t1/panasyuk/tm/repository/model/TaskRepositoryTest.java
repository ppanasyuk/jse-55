package ru.t1.panasyuk.tm.repository.model;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.model.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.model.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория задач на графах")
public class TaskRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User testUser1;

    @NotNull
    private User testUser2;

    @NotNull
    private Project project1;

    @NotNull
    private Project project2;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws SQLException, IOException, LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        userRepository = context.getBean(IUserRepository.class);
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        testUser1 = new User();
        testUser2 = new User();
        userRepository.add(testUser1);
        userRepository.add(testUser2);
        entityManager.getTransaction().commit();
        projectRepository = context.getBean(IProjectRepository.class);
        entityManager = projectRepository.getEntityManager();
        entityManager.getTransaction().begin();
        project1 = new Project();
        project2 = new Project();
        projectRepository.add(project1);
        projectRepository.add(project2);
        taskRepository = context.getBean(ITaskRepository.class);
        entityManager.getTransaction().commit();
        entityManager = taskRepository.getEntityManager();
        entityManager.getTransaction().begin();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i < 5) {
                task.setUser(testUser1);
                task.setProject(project1);
            } else {
                task.setUser(testUser2);
                task.setProject(project2);
            }
            taskList.add(task);
            taskRepository.add(task);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void afterTest() {
        taskRepository.clear();
        entityManager.getTransaction().commit();
        entityManager = projectRepository.getEntityManager();
        entityManager.getTransaction().begin();
        projectRepository.clear();
        entityManager.getTransaction().commit();
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    @DisplayName("Добавление задачи")
    public void addTest() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String taskName = "Task Name";
        @NotNull final String taskDescription = "Task Description";
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUser(testUser1);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task createdTask = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(testUser1, createdTask.getUser());
        Assert.assertEquals(taskName, createdTask.getName());
        Assert.assertEquals(taskDescription, createdTask.getDescription());
    }

    @Test
    @DisplayName("Добавление списка задач")
    public void addAllTest() {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "First Task Name";
        @NotNull final String firstTaskDescription = "Task Description";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "Second Task Name";
        @NotNull final String secondTaskDescription = "Task Description";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        tasks.add(secondTask);
        @NotNull final Collection<Task> addedTasks = taskRepository.add(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить все задачи для пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(testUser1.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(testUser1.getId()));
    }

    @Test
    @DisplayName("Поиск всех задач")
    public void findAllTest() {
        @Nullable final List<Task> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @DisplayName("Поиск всех задач по Id проекта")
    public void findAllByProjectIdTest() {
        @NotNull List<Task> taskListForProject = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .filter(m -> project1.equals(m.getProject()))
                .collect(Collectors.toList());
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(testUser1.getId(), project1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForProject, tasks);
    }

    @Test
    @DisplayName("Поиск всех задач по компаратору")
    public void findAllWithComparatorTest() {
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = taskRepository.findAll().size();
        @Nullable List<Task> tasks = taskRepository.findAll(comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(allProjectsSize, tasks.size());
    }

    @Test
    @DisplayName("Поиск всех задач для пользователя")
    public void findAllForUserTest() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Task> tasks = taskRepository.findAll(testUser1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Поиск всех задач для пользователя по компаратору")
    public void findAllWithComparatorForUser() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .filter(m -> project1.equals(m.getProject()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<Task> tasks = taskRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Поиск задачи по Id")
    public void findOneByIdTest() {
        @Nullable Task task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Поиск задачи по Id равному Null")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final Task foundTask = taskRepository.findOneById("qwerty");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    @DisplayName("Поиск задачи по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable Task task;
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            task = taskListForUser.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(testUser1.getId(), taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Поиск задачи по Id равному Null для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final Task foundTask = taskRepository.findOneById(testUser1.getId(), "qwerty");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(testUser1.getId(), null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    @DisplayName("Поиск задачи по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= taskList.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Поиск задачи по индексу равному Null")
    public void findOneByIndexNullTest() throws Exception {
        @Nullable final Task task = taskRepository.findOneByIndex(null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Поиск задачи по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= taskListForUser.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(testUser1.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Поиск задачи по индексу равному Null")
    public void findOneByIndexNullForUserText() throws Exception {
        @Nullable final Task task = taskRepository.findOneByIndex(testUser1.getId(), null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Получить количество задач")
    public void getSizeTest() throws Exception {
        int actualSize = taskRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество задач для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .count();
        int actualSize = taskRepository.getSize(testUser1.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить задачу")
    public void removeTest() {
        @Nullable final Task task = taskList.get(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final Task deletedTask = taskRepository.remove(task);
        Assert.assertNotNull(deletedTask);
        @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
        Assert.assertNull(deletedTaskInRepository);
    }

    @Test
    @DisplayName("Удалить Null задачу")
    public void removeNullTest() {
        @Nullable final Task task = taskRepository.remove(null);
        Assert.assertNull(task);
    }

}