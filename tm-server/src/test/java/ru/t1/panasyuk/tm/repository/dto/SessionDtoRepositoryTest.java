package ru.t1.panasyuk.tm.repository.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория сессий DTO")
public class SessionDtoRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private String testUser1Id;

    @NotNull
    private String testUser2Id;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private ISessionDtoRepository sessionRepository;

    @NotNull
    private IUserDtoRepository userRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws SQLException, IOException, LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        userRepository = context.getBean(IUserDtoRepository.class);
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final UserDTO user1 = new UserDTO();
        testUser1Id = user1.getId();
        @NotNull final UserDTO user2 = new UserDTO();
        testUser2Id = user2.getId();
        userRepository.add(user1);
        userRepository.add(user2);
        sessionRepository = context.getBean(ISessionDtoRepository.class);
        entityManager.getTransaction().commit();
        entityManager = sessionRepository.getEntityManager();
        entityManager.getTransaction().begin();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            if (i < 5) session.setUserId(testUser1Id);
            else session.setUserId(testUser2Id);
            session.setRole(Role.USUAL);
            sessionList.add(session);
            sessionRepository.add(session);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void afterTest() {
        sessionRepository.clear();
        entityManager.getTransaction().commit();
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    @DisplayName("Добавить сессию")
    public void addTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setRole(Role.USUAL);
        sessionRepository.add(testUser1Id, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final SessionDTO createdSession = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(testUser1Id, createdSession.getUserId());
    }

    @Test
    @DisplayName("Добавить Null сессию")
    public void addNullTest() throws Exception {
        @Nullable final SessionDTO createdSessionDTO = sessionRepository.add(testUser1Id, null);
        Assert.assertNull(createdSessionDTO);
    }

    @Test
    @DisplayName("Добавить список сессий")
    public void addAllTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 2;
        @NotNull final List<SessionDTO> sessions = new ArrayList<>();
        @NotNull final SessionDTO firstSessionDTO = new SessionDTO();
        firstSessionDTO.setRole(Role.USUAL);
        sessions.add(firstSessionDTO);
        @NotNull final SessionDTO secondSessionDTO = new SessionDTO();
        secondSessionDTO.setRole(Role.USUAL);
        sessions.add(secondSessionDTO);
        @NotNull final Collection<SessionDTO> addedSessionDTOs = sessionRepository.add(sessions);
        Assert.assertTrue(addedSessionDTOs.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить все сессии для пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear(testUser1Id);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(testUser1Id));
    }

    @Test
    @DisplayName("Поиск всех сессий")
    public void findAllTest() throws Exception {
        @Nullable final List<SessionDTO> sessions = sessionRepository.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertTrue(sessions.size() > 0);
    }

    @Test
    @DisplayName("Поиск всех сессий для пользователя")
    public void findAllForUserTest() {
        @NotNull List<SessionDTO> sessionListForUser = sessionList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<SessionDTO> sessions = sessionRepository.findAll(testUser1Id);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionListForUser.size(), sessions.size());
    }

    @Test
    @DisplayName("Поиск сессии по Id")
    public void findOneByIdTest() {
        @Nullable SessionDTO session;
        for (int i = 0; i < sessionList.size(); i++) {
            session = sessionList.get(i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById(sessionId);
            Assert.assertNotNull(foundSessionDTO);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById("qwerty");
        Assert.assertNull(foundSessionDTO);
        @Nullable final SessionDTO foundSessionDTONull = sessionRepository.findOneById(null);
        Assert.assertNull(foundSessionDTONull);
    }

    @Test
    @DisplayName("Поиск сессии по Id для пользователя")
    public void findOneByIdForUserTest() {
        @Nullable SessionDTO session;
        @NotNull List<SessionDTO> sessionListForUser = sessionList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionListForUser.size(); i++) {
            session = sessionRepository.findOneByIndex(testUser1Id, i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById(testUser1Id, sessionId);
            Assert.assertNotNull(foundSessionDTO);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null Id для пользователя")
    public void findOneByIdNullForUserTest() {
        @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById(testUser1Id, "qwerty");
        Assert.assertNull(foundSessionDTO);
        @Nullable final SessionDTO foundSessionDTONull = sessionRepository.findOneById(testUser1Id, null);
        Assert.assertNull(foundSessionDTONull);
    }

    @Test
    @DisplayName("Поиск сессии по индексу")
    public void findOneByIndexTest() {
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(1);
        Assert.assertNotNull(session);
    }

    @Test
    @DisplayName("Поиск сессии по Null индексу")
    public void findOneByIndexNullTest() {
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Поиск сессии по индексу для пользователя")
    public void findOneByIndexForUserTest() {
        @NotNull List<SessionDTO> sessionListForUser = sessionList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionListForUser.size(); i++) {
            @Nullable final SessionDTO session = sessionRepository.findOneByIndex(testUser1Id, i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null индексу для пользователя")
    public void findOneByIndexNullForUserText() {
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(testUser1Id, null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Получение количества сессий")
    public void getSizeTest() throws Exception {
        int actualSize = sessionRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получение количества сессий для пользователя")
    public void getSizeForUserTest() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .count();
        int actualSize = sessionRepository.getSize(testUser1Id);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удаление сессии")
    public void removeTest() {
        @Nullable final SessionDTO session = sessionList.get(1);
        Assert.assertNotNull(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final SessionDTO deletedSessionDTO = sessionRepository.remove(session);
        Assert.assertNotNull(deletedSessionDTO);
        @Nullable final SessionDTO deletedSessionDTOInRepository = sessionRepository.findOneById(sessionId);
        Assert.assertNull(deletedSessionDTOInRepository);
    }

    @Test
    @DisplayName("Удаление Null сессии")
    public void removeNullTest() throws Exception {
        @Nullable final SessionDTO session = sessionRepository.remove(null);
        Assert.assertNull(session);
    }

}