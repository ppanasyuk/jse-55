package ru.t1.panasyuk.tm.service.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.api.service.dto.IProjectDtoService;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.api.service.dto.ISessionDtoService;
import ru.t1.panasyuk.tm.api.service.dto.IUserDtoService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса SessionDtoService")
public class SessionDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private ISessionDtoService sessionService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private UserDTO test;

    @NotNull
    private UserDTO admin;

    @NotNull
    private UserDTO user;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskDtoService projectTaskService = context.getBean(IProjectTaskDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        sessionService = context.getBean(ISessionDtoService.class);
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        user = userService.create("USER", "USER", "USER@TEST.ru", Role.USUAL);
        @NotNull final SessionDTO session1 = new SessionDTO();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        sessionService.add(test.getId(), session1);
        @NotNull final SessionDTO session2 = new SessionDTO();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        sessionService.add(admin.getId(), session2);
        @NotNull final SessionDTO session3 = new SessionDTO();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionService.add(user.getId(), session3);
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
        sessionList.add(session3);
    }

    @After
    public void afterTest() throws Exception {
        sessionService.clear(test.getId());
        sessionService.clear(user.getId());
        sessionService.clear(admin.getId());
        userService.remove(admin);
        userService.remove(test);
        userService.remove(user);
    }

    @Test
    @DisplayName("Добавление сессии для пользователя")
    public void AddForUserTest() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize(test.getId()) + 1;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(test.getId());
        session.setRole(test.getRole());
        sessionService.add(test.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление Null сессии для пользователя")
    public void AddNullForUserTest() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize(test.getId());
        @Nullable final SessionDTO session = sessionService.add(test.getId(), null);
        Assert.assertNull(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Удалить все сессии для пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize(test.getId()) > 0);
        sessionService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Проверить существование сессии по Id")
    public void existByIdTrueTest() throws Exception {
        for (@NotNull final SessionDTO session : sessionList) {
            Assert.assertNotNull(session.getUserId());
            final boolean isExist = sessionService.existsById(session.getUserId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверить существование сессии по Id для пользователя")
    public void existByIdTrueForUserTest() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final SessionDTO session : sessionsForTestUser) {
            final boolean isExist = sessionService.existsById(test.getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверить несуществование сессии по Id для пользователя")
    public void existByIdFalseUserTest() throws Exception {
        final boolean isExist = sessionService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Найти все сессии для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<SessionDTO> sessions = sessionService.findAll(test.getId());
        Assert.assertEquals(sessionsForTestUser.size(), sessions.size());
    }

    @Test
    @DisplayName("Найти сессию по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable SessionDTO foundSession;
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final SessionDTO session : sessionsForTestUser) {
            foundSession = sessionService.findOneById(test.getId(), session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Найти сессию по Null Id для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final SessionDTO foundSession = sessionService.findOneById(test.getId(), null);
        Assert.assertNull(foundSession);
    }

    @Test
    @DisplayName("Найти сессию по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionsForTestUser.size(); i++) {
            @Nullable final SessionDTO session = sessionService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по индексу превышающему число сессий для пользователя")
    public void findOneByIndexForUserIndexIncorrectNegative() throws Exception {
        int index = sessionService.getSize(test.getId()) + 1;
        @Nullable final SessionDTO session = sessionService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по Null индексу для пользователя")
    public void findOneByIndexForUserNullIndexIncorrectNegative() throws Exception {
        @Nullable final SessionDTO session = sessionService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по отрицательному индексу для пользователя")
    public void findOneByIndexForUserMinusIndexIncorrectNegative() throws Exception {
        @Nullable final SessionDTO session = sessionService.findOneByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Получить количество сессий для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = sessionService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить сессию")
    public void removeTest() throws Exception {
        for (@NotNull final SessionDTO session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO deletedSession = sessionService.remove(session);
            Assert.assertNotNull(deletedSession);
            Assert.assertNotNull(session.getUserId());
            @Nullable final SessionDTO deletedSessionInRepository = sessionService.findOneById(session.getUserId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить Null сессию")
    public void removeEntityNullNotFoundTestNegative() throws Exception {
        @Nullable final SessionDTO deletedSession = sessionService.remove(null);
    }

    @Test
    @DisplayName("Удалить сессию по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final SessionDTO session : sessionsForTestUser) {
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO deletedSession = sessionService.removeById(test.getId(), sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final SessionDTO deletedSessionInRepository = sessionService.findOneById(test.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить сессию по Null Id для пользователя")
    public void removeByIdForUserIdNullTestNegative() throws Exception {
        sessionService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить сессию по пустому Id для пользователя")
    public void removeByIdForUserIdEmptyTestNegative() throws Exception {
        sessionService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую сессию по Id для пользователя")
    public void removeByIdForUserEntityNotFoundTestNegative() throws Exception {
        sessionService.removeById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить сессию по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final SessionDTO deletedSession = sessionService.removeByIndex(test.getId(), index);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final SessionDTO deletedSessionInRepository = sessionService.findOneById(test.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по индексу превышающему количество сессий для пользователя")
    public void removeByIndexForUserIndexIncorrectTestNegative() throws Exception {
        int index = sessionList.size() + 1;
        sessionService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по Null индексу для пользователя")
    public void removeByIndexNullForUserIndexIncorrectTestNegative() throws Exception {
        sessionService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по отрицательному индексу для пользователя")
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() throws Exception {
        sessionService.removeByIndex(test.getId(), -1);
    }

}