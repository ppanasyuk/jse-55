package ru.t1.panasyuk.tm.migration;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;

@DisplayName("Тестирование создания схемы")
public class SchemeTest extends AbstractSchemeTest {

    @Test
    @DisplayName("Создание схемы")
    public void test() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final ProjectDTO project = createProject();
        Assert.assertNotNull(project);
        int countOfProjects = getCountOfProjects();
        Assert.assertEquals(1, countOfProjects);
        deleteProject(project);
    }

    @NotNull
    private ProjectDTO createProject() {
        @NotNull final IProjectDtoRepository projectRepository = context.getBean(IProjectDtoRepository.class);
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectRepository.add(projectDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projectDTO;
    }

    private int getCountOfProjects() {
        @NotNull final IProjectDtoRepository projectRepository = context.getBean(IProjectDtoRepository.class);
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        entityManager.getTransaction().begin();
        final int count = projectRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteProject(@NotNull final ProjectDTO project) {
        @NotNull final IProjectDtoRepository projectRepository = context.getBean(IProjectDtoRepository.class);
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        entityManager.getTransaction().begin();
        projectRepository.remove(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}