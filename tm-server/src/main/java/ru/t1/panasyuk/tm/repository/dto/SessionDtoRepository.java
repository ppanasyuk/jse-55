package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    @NotNull
    @Override
    protected Class<SessionDTO> getEntityClass() {
        return SessionDTO.class;
    }

}