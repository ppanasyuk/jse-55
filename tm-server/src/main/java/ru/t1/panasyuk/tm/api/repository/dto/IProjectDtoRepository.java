package ru.t1.panasyuk.tm.api.repository.dto;

import ru.t1.panasyuk.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}