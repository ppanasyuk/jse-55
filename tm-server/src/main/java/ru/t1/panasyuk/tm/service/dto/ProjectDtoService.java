package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.IProjectDtoService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IProjectDtoRepository>
        implements IProjectDtoService {

    @NotNull
    @Override
    protected IProjectDtoRepository getRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDTO create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        return add(userId, project);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<ProjectDTO> models;
        @NotNull final IProjectDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            models = repository.findAll(userId, comparator);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<ProjectDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

}