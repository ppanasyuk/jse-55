package ru.t1.panasyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.t1.panasyuk.tm.api.endpoint.ISystemEndpoint;
import ru.t1.panasyuk.tm.dto.request.system.*;
import ru.t1.panasyuk.tm.dto.response.system.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.panasyuk.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(getPropertyService().getAuthorEmail());
        response.setName(getPropertyService().getAuthorName());
        response.setApplicationName(getPropertyService().getApplicationName());
        response.setGitBranch(getPropertyService().getGitBranch());
        response.setGitCommitId(getPropertyService().getGitCommitId());
        response.setGitCommitMessage(getPropertyService().getGitCommitMessage());
        response.setGitCommitterName(getPropertyService().getGitCommitterName());
        response.setGitCommitterEmail(getPropertyService().getGitCommitterEmail());
        response.setGitCommitTime(getPropertyService().getGitCommitTime());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(getPropertyService().getApplicationVersion());
        return response;
    }

}