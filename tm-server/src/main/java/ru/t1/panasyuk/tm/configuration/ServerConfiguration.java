package ru.t1.panasyuk.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.panasyuk.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String driver = propertyService.getDBDriver();
        @NotNull final String dialect = propertyService.getDBDialect();
        @NotNull final String hbm2DdlAuto = propertyService.getDBHbm2DdlAuto();
        @NotNull final Boolean showSql = propertyService.getDBLoggingEnabled();
        @NotNull final String lazyLoadNoTransEnabled = propertyService.getDBLazyLoadNoTransEnabled();
        @NotNull final String formatSql = propertyService.getDBFormatSql();
        @NotNull final String secondLevelCache = propertyService.getDBSecondLevelCacheEnabled();
        @NotNull final String cacheRegionFactory = propertyService.getDBCacheRegionFactory();
        @NotNull final String useQueryCache = propertyService.getDBUseQueryCache();
        @NotNull final String useMinimalPuts = propertyService.getDBUseMinimalPuts();
        @NotNull final String cacheRegionPrefix = propertyService.getDBCacheRegionPrefix();
        @NotNull final String configFilePath = propertyService.getDBConfigFilePath();
        settings.put(Environment.DRIVER, driver);
        settings.put(Environment.URL, url);
        settings.put(Environment.USER, username);
        settings.put(Environment.PASS, password);
        settings.put(Environment.DIALECT, dialect);
        settings.put(Environment.HBM2DDL_AUTO, hbm2DdlAuto);
        settings.put(Environment.SHOW_SQL, showSql.toString());
        settings.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, lazyLoadNoTransEnabled);
        settings.put(Environment.FORMAT_SQL, formatSql);
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, secondLevelCache);
        settings.put(Environment.CACHE_REGION_FACTORY, cacheRegionFactory);
        settings.put(Environment.USE_QUERY_CACHE, useQueryCache);
        settings.put(Environment.USE_MINIMAL_PUTS, useMinimalPuts);
        settings.put(Environment.CACHE_REGION_PREFIX, cacheRegionPrefix);
        settings.put(Environment.CACHE_PROVIDER_CONFIG, configFilePath);
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry standardServiceRegistry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(standardServiceRegistry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}