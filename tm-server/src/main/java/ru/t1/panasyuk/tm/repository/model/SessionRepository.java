package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.api.repository.model.ISessionRepository;
import ru.t1.panasyuk.tm.model.Session;

@Repository
@Scope("prototype")
public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    @Override
    protected Class<Session> getEntityClass() {
        return Session.class;
    }

}