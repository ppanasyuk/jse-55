package ru.t1.panasyuk.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.model.IUserRepository;
import ru.t1.panasyuk.tm.api.service.model.IProjectService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    private IUserRepository getUserRepository() {
        return context.getBean(IUserRepository.class);
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @NotNull final Project project = new Project();
        try {
            entityManager.getTransaction().begin();
            @Nullable final User user = userRepository.findOneById(userId);
            project.setName(name);
            project.setDescription(description);
            project.setUser(user);
            add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @NotNull final Project project = new Project();
        try {
            entityManager.getTransaction().begin();
            @Nullable final User user = userRepository.findOneById(userId);
            project.setName(name);
            project.setUser(user);
            add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<Project> models;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            models = repository.findAll(userId, comparator);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<Project> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @NotNull
    @Override
    public Project updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

}