package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.api.service.dto.IUserDtoService;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.UserNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;

@Service
public final class UserDtoService extends AbstractDtoService<UserDTO, IUserDtoRepository>
        implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    @Override
    protected IUserDtoRepository getRepository() {
        return context.getBean(IUserDtoRepository.class);
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email != null && !email.isEmpty() && isEmailExist(email)) throw new EmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user;
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = repository.findByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        @Nullable UserDTO user;
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = repository.findByEmail(email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO remove(@Nullable final UserDTO user) {
        if (user == null) throw new EntityNotFoundException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectTaskService.clearProjects(user.getId());
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> users) {
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            for (@NotNull final UserDTO user : users)
                repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return users;
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

}