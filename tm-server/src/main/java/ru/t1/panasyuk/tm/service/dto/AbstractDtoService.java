package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.repository.dto.IDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.IDtoService;
import ru.t1.panasyuk.tm.dto.model.AbstractModelDTO;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>>
        implements IDtoService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        boolean result;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            result = repository.findOneById(id) != null;
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            models = repository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        @Nullable final M model;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            model = repository.findOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable final M model;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            model = repository.findOneByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public int getSize() {
        int result;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            result = repository.getSize();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M remove(@NotNull final M model) {
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            removedModel = repository.findOneById(model.getId());
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            removedModel = repository.findOneById(id);
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            removedModel = repository.findOneByIndex(index);
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

}