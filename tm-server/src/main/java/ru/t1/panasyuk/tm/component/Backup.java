package ru.t1.panasyuk.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.api.service.IDomainService;
import ru.t1.panasyuk.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private IDomainService domainService;

    public void start() {
        es.scheduleWithFixedDelay(this::save, 0, 10, TimeUnit.MINUTES);
    }

    public void save() {
        domainService.saveDataBackup();
    }

    public void stop() {
        es.shutdown();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        domainService.loadDataBackup();
    }

}
