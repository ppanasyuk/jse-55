package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataJsonSaveFasterXMLRequest;

@Component
public final class DataJsonSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in json file.";

    @NotNull
    private static final String NAME = "data-save-json-fasterxml";


    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXMLRequest request = new DataJsonSaveFasterXMLRequest(getToken());
        domainEndpoint.saveDataJsonFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
