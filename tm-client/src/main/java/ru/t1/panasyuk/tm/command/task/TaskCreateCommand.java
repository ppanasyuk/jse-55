package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskCreateRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskCreateResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Create new task.";

    @NotNull
    private static final String NAME = "task-create";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), name, description);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}