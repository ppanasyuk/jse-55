package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.panasyuk.tm.dto.request.project.ProjectUpdateByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectUpdateByIndexResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Update project by index.";

    @Nullable
    private static final String NAME = "project-update-by-index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken(), index, name, description);
        @NotNull final ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}