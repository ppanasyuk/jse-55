package ru.t1.panasyuk.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.system.ServerAboutRequest;
import ru.t1.panasyuk.tm.dto.response.system.ServerAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        ServerAboutResponse response = systemEndpoint.getAbout(request);

        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
        System.out.println("Application Name: " + response.getApplicationName());

        System.out.println("[GIT]");
        System.out.println("Branch: " + response.getGitBranch());
        System.out.println("Commit Id: " + response.getGitCommitId());
        System.out.println("Committer: " + response.getGitCommitterName());
        System.out.println("E-mail: " + response.getGitCommitterEmail());
        System.out.println("Message: " + response.getGitCommitMessage());
        System.out.println("Time: " + response.getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}