package ru.t1.panasyuk.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.command.user.UserLogoutCommand;
import ru.t1.panasyuk.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.panasyuk.tm.exception.system.CommandNotSupportedException;
import ru.t1.panasyuk.tm.util.SystemUtil;
import ru.t1.panasyuk.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    private void exit() {
        System.exit(0);
    }

    private void initCommands() {
        for (@Nullable final AbstractCommand command : abstractCommands) {
            if (command == null) return;
            registry(command);
        }
    }

    private void initFileScanner() {
        fileScanner.start();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareStartup() {
        initPID();
        initLogger();
        initFileScanner();
    }

    private void prepareShutdown() {
        processCommand(UserLogoutCommand.NAME);
        fileScanner.stop();
        loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***");
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        if (arguments[0] == null) return;
        processArgument(arguments[0]);
        exit();
    }

    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        commandService.add(command);
    }

    public void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
        loggerService.command(command);
    }

    public void run(@Nullable final String... args) {
        initCommands();
        processArguments(args);
        prepareStartup();
        processCommands();
    }

}