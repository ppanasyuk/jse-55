package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskStartByIdResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Start task by id.";

    @NotNull
    private static final String NAME = "task-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken(), id);
        @NotNull final TaskStartByIdResponse response = taskEndpoint.startTaskById(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}