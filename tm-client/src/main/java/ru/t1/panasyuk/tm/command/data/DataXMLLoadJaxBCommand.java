package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataXMLLoadJaxBRequest;

@Component
public final class DataXMLLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    private static final String NAME = "data-load-xml-jaxb";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXMLLoadJaxBRequest request = new DataXMLLoadJaxBRequest(getToken());
        domainEndpoint.loadDataXMLJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
