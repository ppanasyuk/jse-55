package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskUpdateByIndexResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Update task by index.";

    @NotNull
    private static final String NAME = "task-update-by-index";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken(), index, name, description);
        @NotNull final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}