package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProject extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    private static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken(), projectId, taskId);
        @NotNull final TaskUnbindFromProjectResponse response = taskEndpoint.unbindTaskFromProject(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}