package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.project.ProjectFindOneByIdRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectFindOneByIdResponse;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show project by id.";

    @NotNull
    private static final String NAME = "project-show-by-id";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(getToken(), id);
        @NotNull final ProjectFindOneByIdResponse response = projectEndpoint.findProjectById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}