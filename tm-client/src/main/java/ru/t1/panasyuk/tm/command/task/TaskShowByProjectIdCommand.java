package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskFindAllByProjectIdRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskFindAllByProjectIdResponse;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskFindAllByProjectIdRequest request = new TaskFindAllByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskFindAllByProjectIdResponse response = taskEndpoint.findAllByProjectId(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}