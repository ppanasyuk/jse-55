package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectStartByIndexResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Start project by index.";

    @NotNull
    private static final String NAME = "project-start-by-index";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken(), index);
        @NotNull final ProjectStartByIndexResponse response = projectEndpoint.startProjectByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}