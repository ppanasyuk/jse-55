package ru.t1.panasyuk.tm.repository;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.repository.ICommandRepository;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.command.system.ApplicationAboutCommand;
import ru.t1.panasyuk.tm.command.system.ApplicationVersionCommand;
import ru.t1.panasyuk.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitCategory.class)
@DisplayName("Тестирование репозитория команд")
public class CommandRepositoryTest {

    @NotNull
    private ICommandRepository commandRepository;

    @NotNull
    private AbstractCommand testCommand;

    @Before
    public void initRepository() {
        commandRepository = new CommandRepository();
        testCommand = new ApplicationAboutCommand();
        commandRepository.add(testCommand);
    }

    @Test
    @DisplayName("Добавление команды")
    public void addTest() {
        @NotNull final AbstractCommand command = new ApplicationVersionCommand();
        commandRepository.add(command);
        @Nullable final AbstractCommand newCommand = commandRepository.getCommandByName(command.getName());
        Assert.assertNotNull(newCommand);
        Assert.assertEquals(command, newCommand);
    }

    @Test
    @DisplayName("Добавление Null команды")
    public void addNullTest() {
        int numberOfTerminalCommands = commandRepository.getTerminalCommands().size();
        commandRepository.add(null);
        Assert.assertEquals(numberOfTerminalCommands, commandRepository.getTerminalCommands().size());
    }

    @Test
    @DisplayName("Получение команды по аргументу")
    public void getCommandByArgumentTest() {
        @Nullable final String argument = testCommand.getArgument();
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByArgument(argument);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по аргументу равному Null")
    public void getCommandByArgumentNullTest() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByArgument(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по пустому аргументу")
    public void getCommandByArgumentEmptyTest() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByArgument("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по наименованию")
    public void getCommandByNameTest() {
        @Nullable final String name = testCommand.getName();
        Assert.assertNotNull(name);
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByName(name);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по наименованию равному Null")
    public void getCommandByNameNullTest() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByName(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по пустому наименованию")
    public void getCommandByNameEmptyTest() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByName("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команд с аргументом")
    public void getCommandsWithArgument() {
        @NotNull final Iterable<AbstractCommand> commands = commandRepository.getCommandsWithArgument();
        @NotNull final List<AbstractCommand> commandList = new ArrayList<>();
        for (AbstractCommand command : commands) commandList.add(command);
        Assert.assertTrue(commandList.size() > 0);
    }

    @Test
    @DisplayName("Получение всех команд")
    public void getTerminalCommands() {
        @NotNull final Collection<AbstractCommand> commands = commandRepository.getTerminalCommands();
        Assert.assertTrue(commands.size() > 0);
    }

}