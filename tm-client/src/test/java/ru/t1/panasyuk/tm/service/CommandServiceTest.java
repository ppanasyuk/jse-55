package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.api.repository.ICommandRepository;
import ru.t1.panasyuk.tm.api.service.ICommandService;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.command.system.ApplicationAboutCommand;
import ru.t1.panasyuk.tm.command.system.ApplicationVersionCommand;
import ru.t1.panasyuk.tm.configuration.ClientConfiguration;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.marker.UnitCategory;
import ru.t1.panasyuk.tm.repository.CommandRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(SoapCategory.class)
@DisplayName("Тестирование сервиса команд")
public class CommandServiceTest {

    @NotNull
    private ICommandService commandService;

    @NotNull
    private AbstractCommand testCommand;

    @NotNull
    private static ApplicationContext context;

    @BeforeClass
    public static void initTest() {
        context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
    }

    @Before
    public void initRepository() {
        @NotNull final ICommandRepository commandRepository = new CommandRepository();
        commandService = context.getBean(ICommandService.class);
        testCommand = new ApplicationAboutCommand();
        commandService.add(testCommand);
    }

    @Test
    @DisplayName("Добавление команды")
    public void addTest() {
        @NotNull final AbstractCommand command = new ApplicationVersionCommand();
        commandService.add(command);
        @Nullable final AbstractCommand newCommand = commandService.getCommandByName(command.getName());
        Assert.assertNotNull(newCommand);
        Assert.assertEquals(command, newCommand);
    }

    @Test
    @DisplayName("Добавление Null команды")
    public void addNullTest() {
        int numberOfTerminalCommands = commandService.getTerminalCommands().size();
        commandService.add(null);
        Assert.assertEquals(numberOfTerminalCommands, commandService.getTerminalCommands().size());
    }

    @Test
    @DisplayName("Получение команды по аргументу")
    public void getCommandByArgumentTest() {
        @Nullable final String argument = testCommand.getArgument();
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByArgument(argument);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по аргументу равному Null")
    public void getCommandByArgumentNullTest() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByArgument(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по пустому аргументу")
    public void getCommandByArgumentEmptyTest() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByArgument("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по наименованию")
    public void getCommandByNameTest() {
        @Nullable final String name = testCommand.getName();
        Assert.assertNotNull(name);
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByName(name);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по наименованию равному Null")
    public void getCommandByNameNullTest() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByName(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команды по пустому наименованию")
    public void getCommandByNameEmptyTest() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByName("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    @DisplayName("Получение команд с аргументом")
    public void getCommandsWithArgument() {
        @NotNull final Iterable<AbstractCommand> commands = commandService.getCommandsWithArgument();
        @NotNull final List<AbstractCommand> commandList = new ArrayList<>();
        for (AbstractCommand command : commands) commandList.add(command);
        Assert.assertTrue(commandList.size() > 0);
    }

    @Test
    @DisplayName("Получение всех команд")
    public void getTerminalCommands() {
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        Assert.assertTrue(commands.size() > 0);
    }

}